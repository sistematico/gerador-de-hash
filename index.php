<?php
// Catch cURL/Wget requests
//if (isset($_POST['envio']) && !empty($_POST['envio']) || isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/^(curl|wget)/i', $_SERVER['HTTP_USER_AGENT'])) {
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    || isset($_SERVER['HTTP_USER_AGENT'])
    && preg_match('/^(curl|wget)/i', $_SERVER['HTTP_USER_AGENT'])
) {
//    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        if (isset($_POST["senha"]) && !empty($_POST["senha"])) {
            echo password_hash($_POST["senha"], PASSWORD_DEFAULT);
        }
//    }

    exit();
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Gerador de Hash</title>

        <meta name="description" content="Gerador de Hash">
        <meta name="keywords" content="Gerador de Hash, Hash">
        <meta name="author" content="Lucas Saliés Brum">

        <meta property="og:title" content="Gerador de Hash" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://hash.lucasbrum.net" />
        <meta property="og:image" content="https://hash.lucasbrum.net/img/hash.png" />

        <link rel="stylesheet" href="css/bulma.min.css">
        <link rel="stylesheet" href="css/fontawesome-5.11.2.min.css">
        <link rel="stylesheet" href="css/bulma-switch.min.css">
        <link rel="stylesheet" href="css/principal.css">

        <link rel="shortcut icon" href="img/favicon.ico">
    </head>
    <body>

        <section class="hero is-info is-fullheight">
            <div class="hero-body">

                <div class="container">

                    <div class="columns is-centered">
                        <div class="column is-half">

                            <div class="tile is-ancestor">
                                <div class="tile is-parent">
                                    <article class="tile is-child">
                                        <p class="title">Gerador de Hash</p>
                                        <p class="subtitle">Encripte sua senha</p>
                                        <form id="formulario" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                                            <div class="field has-addons">
                                                <div class="control">
                                                    <input name="senha" id="senha" class="input" type="text" placeholder="Senha">
                                                </div>
                                                <div class="control">
                                                    <input class="button is-success" type="submit" name="envio" value="Gerar" />
                                                </div>
                                            </div>
                                            <div class="field has-addons">
                                                <div class="control">
                                                    <input id="hash" class="input" type="text" placeholder="Hash">
                                                </div>
                                                <div class="control">
                                                    <a class="button is-primary copiar" data-clipboard-target="#hash">Copiar</a>
                                                </div>
                                            </div>
                                        </form>
                                        <br />
                                        <p class="is-size-7 is-italic">
                                            Feito por <a href="https://lucasbrum.net">Lucas Saliés Brum</a>, código fonte no <a href="https://gitlab.com/sistematico/gerador-de-hash">GitLab</a>.
                                        </p>
                                    </article>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </section>
        <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="js/clipboard.min.js"></script>
        <script src="js/script.js"></script>
    </body>
</html>