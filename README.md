# Gerador de Hash

Um gerador de hash para senhas.

## Uso

`curl --data "senha=sua_senha" https://hash.lucasbrum.net`

## Demo

- [Gerador de Hash](https://hash.lucasbrum.net)

## Créditos

- [ClipboardJS](https://clipboardjs.com/)
- Milhares de outros colaboradores...

## Ajude

Doe qualquer valor através do <a href="https://pag.ae/bfxkQW"><img src="https://img.shields.io/badge/pagseguro-green"></a> ou <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWHJL387XNW96&source=url"><img src="https://img.shields.io/badge/paypal-blue"></a>

## ScreenShot

![Screenshot][screenshot]

[screenshot]: https://gitlab.com/sistematico/gerador-de-senhas/raw/master/hash.png "ScreenShot"